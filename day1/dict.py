"""
student1 = {"name": "Nikita", "age": 20}
student2 = {"name": "Parth", "age": 23}
student3 = {"name": "Dhruv", "age": 22}
student4 = {"name": "Arayan", "age": 21}


#print(student3['age'])

students = [student1, student2, student3, student4]

print(students[3]['age'])
"""






users = [
            {"name": "Sachin", "age": 25}, 
            {"name": "Nitin", "age": 17}, 
            {"name": "Vikram", "age": 18}, 
            {"name": "Pushpa", "age": 19}, 
            {"name": "Pathan", "age": 35}, 
            {"name": "Dhinchak Pooja", "age": 26}, 
            {"name": "Badshah", "age": 22}, 
            {"name": "Bantai", "age": 21}, 
            {"name": "Ganesh Gaitonde", "age": 20}, 
            {"name": "Vicky", "age": 29}, 
        ]
"""
#CW WAP CLUB 5 users limit, Condition age greater than or equals 21, allowed to enter in club 
cnt = 0
for user in users:
    if cnt == 5:
        print("Club Full")
        break

    if(user['age'] >= 21):
        print(f"{user['name']} allowed to enter")
        cnt += 1

"""

#WAP to find max, min, average salary of 10 employees [list of dict]
emps = [
        {"name": "Dhruv", "salary": 90000},
        {"name": "Sahil", "salary": 75000},
        {"name": "Nikit", "salary": 73000},
        {"name": "Gayatri", "salary": 80000},
        {"name": "Parth", "salary": 78000},
        {"name": "Aryan", "salary": 90000},
        {"name": "Sanika", "salary": 68000},
        {"name": "Nayan", "salary": 75000},
        {"name": "Vishal", "salary": 88000},
        {"name": "Harsh", "salary": 90000},
]

from prettytable import PrettyTable 
pt = PrettyTable(["Name", "Salary"])

for emp in emps:
    pt.add_row([emp['name'], emp['salary']])
    
    
print(pt)












#dict keys:  name salary
#print all employees having max salary using pretty table module
"""
-------------
|name|salary|
-------------
|ajit|100000|
|anil|100000|
|amit|100000|
-------------
"""