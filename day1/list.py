fruits = ["apple", "banana", "chiku", "orange"]

print(fruits)

print(fruits[2])

print(len(fruits))

#print last element/value of list
print(fruits[len(fruits) - 1])

print("==================")

#WAP to access all values of fruits using for loop
for i in range(0, len(fruits)):
    print(fruits[i])
    
print("==================")

#WAP to access all values of fruits using for loop
for fruit in fruits:
    print(fruit)
    
 
#WAP to print sum of all elements from list nums = [3, 6, 8, 2, 9]

nums = [3, 6, 8, 2, 9]
total = 0

for num in nums:
    total += num    #total = total + num
    
print(total)


nums2 = [1, 2, 3, [4, 5, 6, [7, 8, 9]]]

print(nums2[3][3][1])

#CW WAP to print all eligible age who can drive car
#ages = [["shailesh", 13], ["nikita", 19], 32, 34, 25, 27, 17, 20, 35]