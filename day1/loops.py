"""
WAP to create random jackpot number and take input from user to guess the number. 
Based on level the attempt to guess the number should change
Easy – 20 attempts
Medium – 10 attempts
Difficult – 5 attempts
"""
import random

jackpot = random.randint(1, 100)
attempt = 5
win_flag = False

for i in range(0, attempt):
    guessed_number = int(input("Guess a number: "))

    if guessed_number == jackpot:
        print("You won")
        win_flag = True
        break

    if jackpot > guessed_number:
        print("Hint: Please enter greater number")
        
    if jackpot < guessed_number:
        print("Hint: Please enter smaller number")


if win_flag == False:
    print(f"You have lost the game. Random number was {jackpot}")
    
 



















#LOOPS
"""
WAP to execute Fizz Buzz Problem / Print number 1 to 100
if number is divisible by 3 then print Fizz
if number is divisible by 5 then print Buzz
if number is divisible by both 3 and 5 then print Fizz Buzz

for i in range(1, 101):
    if i % 3 == 0 and i % 5 == 0:
        print("FizzBuzz")
    elif i % 3 == 0:
        print("Fizz")
    elif i % 5 == 0:
        print("Buzz")
    else:
        print(i)

"""


















"""
import time

for floor in range(1, 21):
    if floor == 13:
        break #continue
        
    print(f"Floor No. {floor}")
    
    time.sleep(1)


gn = 73
guess = 50
hint: jackpot number is greater than 50 or enter greater number
"""

"""
import time

for i in range(1, 21):
    if i == 13:
        break; #continue
        
    print(f"Floor No. {i}")
    time.sleep(1)


num = int(input("Enter number: "))
for i in range(1, 11):
    print(f"{num} * {i} = {num * i}\t{num} * {11 - i} = {num * (11-i)}")
"""


#WAP to read a number and print table of it
#5 * 1 = 5      5 * 10 = 50
#5 * 2 = 10     5 * 9 = 45
#....           ...
#5 * 10 = 50    5 * 1 = 5
