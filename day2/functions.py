#10 wrappers return 
#I get 1 chocolate from shopkeeper

#Rs. 50 in pocket
#55 total chocolates i can have

#Rs. 100 in pocket
#Total 111 chocolates i can have

#WAP for this
#getTotalChocolate(10) => 11
#getTotalChocolate(50) => 55
#getTotalChocolate(100) => 111

def getTotalChocolate(num):

    if num < 10:
        return num
        
    return num + getTotalChocolate(num/10)

import math

print(getTotalChocolate(10))
print(getTotalChocolate(50))
print(math.ceil(getTotalChocolate(55)))
print(getTotalChocolate(100))











#Def: A function which calls itself is called recursive function
#NOTE: Make sure you have 1 termination condition in recursive function


"""
def fact(num):

    if num == 1:
        return 1

    return num * fact(num - 1);
    
    
print(fact(3))
print(fact(5))
"""

"""
5! = 5 * 4!
    = 5 * 4 * 3!
    = 5 * 4 * 3 * 2!
    = 5 * 4 * 3 * 2 * 1!

















#FUNCTION WITH DEFAULT ARGUMENT VALUE

def add(num1, num2 = 0, num3 = 0):
    print(f"{num1} + {num2} + {num3} = {num1+num2+num3}")
    

add(4)
add(5, 6)
add(2, 3)
add(7, 8)

add(5, 6, 7)

"""













"""
def greet(first_name, last_name, city):
    print(f"Hello {first_name} {last_name}, How's weather in {city}")
    
    
greet("Tushar", "Kuhite", "Nagpur")

#KEYWORD ARGUMENTS
greet(city="Mumbai", first_name="Dhruv", last_name="Rathi")
"""














"""

def add(x, y):
    #x = 5
    #y = 6
    #print(x + y)
    return x + y


returned_val = add(5, 6)
print(add(returned_val, 7))


print(add(add(1, 2), 3))
print(add(add(1, 2), add(3, 4)))
"""

























#print("Hello Dhruv")
#print("Hello Sanika")

"""
function heading(par1, par2, ...):
    /*
    function
    body
    LOC
    */
   
    return val/object

def greet(name):
    print(f"Hello {name}")
    print(name)
    

greet("Dhruv")
greet("Aditya")
greet("Sanika")

#CW DAF greet(name) which will greet to given name  
"""

#CW DAF add(x, y) and print addition of 3 numbers
#5, 6, 7 add(x, y) 
#return

