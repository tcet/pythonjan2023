#CW Create a class Bank
#Datamember: balance
#Member Function/Method: showBal, deposit(amt), withdraw(amt)

class Bank:
    
    def __init__(self, b):
        self.balance = b
        
    def showBal(self):
        print("Available Balance", self.balance)
        
    def deposit(self, amt):
        self.balance = self.balance + amt
        
    def withdraw(self, amt):
        self.balance = self.balance - amt
        
        
acc1 = Bank(1000)
acc1.showBal()
acc1.deposit(500)
acc1.showBal()
acc1.withdraw(300)
acc1.showBal()

#Create class HDFC(Bank) which will inherit Bank class
#override withdraw method to debit 20 Rs on each transaction

class HDFC(Bank):
    def withdraw(self, amt):
        super().withdraw(amt+20)
        #self.balance = self.balance - (amt + 20)
    

h1 = HDFC(5000)
h1.showBal()
h1.withdraw(1000)
h1.showBal()


#acc2 = Bank(2000)
#acc2.showBal()
























#INHERITANCE: A journey from generalization to specialization 
"""
class Animal:
    
    def breath(self):
        print("Can Breath")
        

a1 = Animal()
a1.breath()

        
class Dog(Animal):
    
    def bark(self):
        print("Can Bark")
        return "Returned value from bark fun"

d1 = Dog()
d1.breath()
ret = d1.bark()
print(ret)
"""


































#CW: DAC Rectangle 
#Data Members: length, breadth
#Find area() and perimeter()
"""
class Rect:
    
    def __init__(self, l = 1, b = 1):
        self.length = l
        self.breadth = b
    
    
    def area(self):
        print(f"Area: {self.length * self.breadth}")
        
    def perimeter(self):
        print(f"Perimeter: {2 * (self.length + self.breadth)}")

        
r1 = Rect(5, 6)
r1.area()
r1.perimeter()

r2 = Rect(7, 8)
r2.area()

r3 = Rect()
r3.area()

"""
























"""
#DAC Circle with data member radius
#Find area() and circumference() of Circle
class Circle:
    
    radius = 1
    
    #constructor
    def __init__(self, r):
        print("Object created successfully")
        self.radius = r
        
    
    def area(self):
        print(f"Area: {3.14 * self.radius * self.radius}")
    

c1 = Circle(5)
print(c1.radius)
c1.area()

c2 = Circle(7)
print(c2.radius)
c2.area()
"""























"""
class Demo:
    clg = "Tcet"
    
    def test(this):
        print("Hello from test")
       
       
    def foo(this):
        print("Hello from foo ")
        
    
    def getClg(this):
        print(f"College Name: {this.clg}")
        
        
    def callAll(this):
        this.test()
        this.foo()
        this.getClg()

obj = Demo()
print(obj)
obj.callAll()
"""


