#CW: create file update.py, search record by name, prompt new salary and update salary by id, display same record

import pymysql
from prettytable import PrettyTable

# To connect MySQL database
conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
cur = conn.cursor(pymysql.cursors.DictCursor)

def updateRecord():
    empid = int(input("Enter id: "))
    salary = int(input("Enter salary: "))
    sql = f"UPDATE employees SET salary = {salary} WHERE id = {empid}"
    print(f"======DEBUG SQL: {sql} ==========")
    cur.execute(sql)
    conn.commit()

def searchRec():
	search = input("Enter name to search: ")
	cur.execute(f"SELECT id, name, salary FROM employees WHERE name LIKE '{search}%'")
	records = cur.fetchall()
	print(records)
	
searchRec();
updateRecord();
searchRec();
