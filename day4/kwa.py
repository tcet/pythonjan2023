def foo(*args, **kwargs):
    print(args);
    print(kwargs);
    
    
foo(5, 6, 7, name="Shailesh", age=32, city="Nagpur");

#CW WAP to read args and print sum of all arguments
#add(5, 6)
#add(5, 6, 7)
#add(1, 2, 3, 4, 5)


def add(*args, **kwargs):
    sum = 0
    for i in args:
        sum += i
        
    print(sum)
    
add(5, 6)
add(5, 6, 7)
add(1, 2, 3, 4, 5)