#CW: create file insert.py and prompt name,city and salary. Insert the same in database using insert sql 
#then display latest 10 records

import pymysql
from prettytable import PrettyTable

# To connect MySQL database
conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
cur = conn.cursor(pymysql.cursors.DictCursor)

def addRecord():
    name = input("Enter name: ")
    city = input("Enter city: ")
    salary = int(input("Enter salary: "))
    sql = f"INSERT INTO employees (id, name, address, salary) VALUE (NULL, '{name}', '{city}', {salary})"
    print(f"======DEBUG SQL: {sql} ==========")
    cur.execute(sql)
    conn.commit()

def showListing():
    cur.execute("SELECT id, name, salary FROM employees ORDER BY id DESC LIMIT 10")
    records = cur.fetchall()
    #print(records)
    pt = PrettyTable(["id", "name", "salary"])
    
    for e in records:
        pt.add_row([e['id'], e['name'], e['salary']])
        

    print(pt)
    
addRecord();
showListing();

#CW: create file update.py, search record by name, prompt new salary and update salary by id, display same record