#CREATE a file delete.py 
#WAP to delete record by employee id
#first show all list of employees id, name
#prompt message Enter id to delete: 123
#delete logic SQL: 
#show all records again

import pymysql

def showListing():
	# To connect MySQL database
	conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
	cur = conn.cursor(pymysql.cursors.DictCursor)
	cur.execute("SELECT id, name FROM employees LIMIT 10")
	records = cur.fetchall()

	print(records)
    
    
def delete():
	# To connect MySQL database
	conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
	cur = conn.cursor(pymysql.cursors.DictCursor)
	empid = int(input("Enter id: "))
	cur.execute(f"DELETE FROM employees WHERE id = {empid}")
	conn.commit()
	
	
showListing();
delete()
showListing();

#CW: create file insert.py and prompt name city and salary. Insert the same in database using insert sql 
#then display latest 10 records