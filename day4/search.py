#CREATE a file search.py 
#WAP to search record by employee name
#Your program should prompt a message 
#Enter name to search: amit
#It should show all records with name amit
#If I enter character a
#Then it should show all records starts with a

import pymysql

# To connect MySQL database
conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')

cur = conn.cursor(pymysql.cursors.DictCursor)

search = input("Enter name to search: ")
cur.execute(f"SELECT id, name FROM employees WHERE name LIKE '{search}%'")
records = cur.fetchall()

print(records)
