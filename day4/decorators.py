from functools import wraps

def my_decorator(f):
    @wraps(f)
    def msg(*args, **kwargs):
        print("I am from custom decorator")
        print("Arguments:", args)
        print("Keyword Arguments:", kwargs)
        
        return f(*args, **kwargs)
        
    return msg;


@my_decorator    
def add(x, y):
    print(f"{x} + {y} = {x + y}")
    
    
add(5, y=10)
add(5, y=10)
add(5, y=10)
add(5, y=10)