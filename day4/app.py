from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def index():
    return "Hello World";
    

@app.route('/sqr')
def getSqr():
    print("=========DEBUGING SQURE FUNCTION============")
    print(f"value of num: {request.args.get('num1')}")
    x = int(request.args.get("num1"))
    return f"Square of {x} is {x * x}";    


@app.route("/add", methods=['POST'])
def add():
    x = int(request.form.get("num1"))
    y = int(request.form.get("num2"))
    return f"{x} + {y} = {x+y}"

#CW Create web service
#add => GET
#sub => POST (form)
#mul => PUT (form)
#div => DELETE (form)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("1234"), debug=True)
    
    
#ON BROWSER hit the url localhost:1234/ or 127.0.0.1:1234/
#Ceate WS add which will add 2 numbers
#http://localhost/add?num1=5&num2=7