import pymysql
from flask import Flask, request, jsonify
from flask_cors import CORS

app = Flask(__name__)
cors = CORS(app)

@app.route('/')
def index():
    return "Hello World";
    

@app.route('/list-employees')
def getEmployeesList():
	# To connect MySQL database
    conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
    cur = conn.cursor(pymysql.cursors.DictCursor)
    cur.execute("SELECT * FROM employees LIMIT 10")
    records = cur.fetchall()

    return jsonify(records)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=1234, debug=True)
    