class Person:
    
    def __init__(self, n, a, c, m):
        self.name = n
        self.age = a
        self.city = c
        self.mobile = m
    
    def showDetails(self):
        print(self.__dict__)
        
#p = Person("Sachin", 23, "Mumbai", 9876543210)
#p.showDetails()

class Employee(Person):
    def __init__(self, n, a, c, m, eid, sal):
        super().__init__(n, a, c, m)
        self.empid = eid
        self.salary = sal

    def getTax(self):
        print(f"Annual Salary: {self.salary * 12}")
        tax = self.salary * 12 * 0.10
        print(f"Annual Tax: {tax}")
        

e = Employee("Nitin", 25, "Thane", 1234567890, 1024, 25000)
e.showDetails()
e.getTax()

#Add method in Employee class to calculate annual tax 10%
#e.getAnnualTax()











class Student(Person):
    def __init__(self, n, a, c, m, rn, ms):
        super().__init__(n, a, c, m)
        self.rollno = rn
        self.marks = ms

#s = Student("Sachin", 23, "Mumbai", 9876543210, 123, 80)
#s.showDetails()





















"""
class Bank:

    #constructor
    def __init__(self, b):
        self.balance = b

    def showBal(self):
        print(f"Available Balance: {self.balance}")

    def deposit(self, amt):
        self.balance = self.balance + amt

    def withdraw(self, amt):
        self.balance -= amt


b = Bank(1000)
b.showBal()
b.deposit(500)
b.showBal()
b.withdraw(200)
b.showBal()


#CW Create a class SBI which will inherit Bank
#Override deposit method to charge 50 rs for each transaction
#Override withdraw method to charge 100 rs for each transaction
"""