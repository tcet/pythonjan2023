#INSERT INTO `users` (`id`, `name`, `age`, `city`) VALUES (NULL, 'arvind', '20', 'nagpur');
#https://codeinsightacademy.com/blog/python/flask-cheat-sheet/
import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
#pip install flask-cors
import pymysql

app = Flask(__name__)
cors = CORS(app)

# To connect MySQL database
conn = pymysql.connect(host='localhost', user='root', password = "", db='tcet')
cur = conn.cursor(pymysql.cursors.DictCursor)

@app.route('/get-users', methods=['GET'])
def get_users():
    sql = "SELECT * FROM users"
    cur.execute(sql)
    result = cur.fetchall()
    return jsonify(result) 
    

@app.route('/add-user', methods=['POST'])
def add_user():
    raw_json = request.get_json()

    print(raw_json)

    name = raw_json['name']
    age = int(raw_json['age'])
    city = raw_json['city']

    sql = f"INSERT INTO users VALUES (NULL, '{name}', '{age}', '{city}')"
    cur.execute(sql)
    conn.commit()
    return "Record added successfully"
    

@app.route("/add", methods=['POST'])
def getAdd():
    raw_json = request.get_json();
    print(raw_json)
    
    x = raw_json['num1']
    y = raw_json['num2']
    
    return jsonify({"result": x + y})

#CW make this web service to read values from raw json
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=1234, debug=True)